        /*
         *Tipos de variables
         * 
         */

        // primitivos e incremento decremento

        var string = "1";
        var bol = true;

        var num = 0;
        var x = ++num;
        console.log("Incremento num = " + num + " x = " + x);
        var num = 0;
        var y = --num;
        console.log("Decremento num = " + num + " y = " + y);

        // Objeto

        var objSobrino = {
            nombre: "matias",
            edad: 6,
            direccion: {
                pais: "chile",
                comuna: "huechuraba"
            }
        };

        var getMati = function () {

            alert(" " + objSobrino.nombre + " es de " + objSobrino.direccion.pais + " de la comuna de " + objSobrino.direccion.comuna);


        }
        //el zip code lo ingresa de igual manera
        console.log(objSobrino);

        objSobrino.direccion.zipcode = 11111;



        //Arreglo

        const a = [1, 2, 3];
        const b = a;
        //agrego algo al array
        a.push("hola");
        console.log(a); // [ 1, 2, 3, 4, "hola" ]
        console.log(b); // [ 1, 2, 3, 4, "hola" ]

        for (const iterator of a) {

            console.log(iterator);
        }


        /*
         *Asincronia
         *Promesas
         */


        //Promesa
        const sumaAfterOneSeconds = (a, b) => {
            return new Promise(resolve => setTimeout(() => resolve(a + b), 1000))
        }

        //Funcion Asincrona
        const sumAll = () => {
            return sumaAfterOneSeconds(1, 1).then(result =>
                sumaAfterOneSeconds(result, 1).then(result => sumaAfterOneSeconds(result, 1))
            )
        }

        sumAll().then(total => console.log("SumAll con asincronia despues de 3 segundos : " + total));

        //Funcion asincrona con -> async() await

        const sumaAsync = async () => {
            const dos = await sumaAfterOneSeconds(1, 1)
            const tres = await sumaAfterOneSeconds(dos, 1)

            return tres;
        }

        sumaAsync().then(totalB => console.log("SumaAsync  Await despues de 2 segundos : " + totalB));

        //Concatenacion

        var one = 1;
        var two = 2;
        var three = '3';

        var result = "".concat(one, two, three); //123

        console.log(result);

        //conversión de variables

        var o;
        var e = o + 2; // Se evalua a NaN ya que no esta defino o
        console.log(e)

        var bol = true;
        bol = String(bol);

        console.log(String(bol) + "boleano String(bol) -> tipo: " + typeof (String(bol)));


        /**
         * Prompt
         * 
         */

        var nombre = prompt("Cual es su nombre");


        if (nombre.length > 2) {
            document.write("Bienvenid@: " + nombre);
        }



        /*
         * switch
         */
        switch (nombre) {
            case nombre = "tuto":

                alert("Hola denuevo tuto");

                break;
            case nombre = "cony":

                alert("Hola Cony, presiona aceptar");

                break;

            case nombre = "matias":

                alert(getMati());

                break;

        }

        /*
         *
         * Ejercio basico con funciones y 2 inputs
         * 
         */



        var suma = function () {

            const uno = parseInt(document.getElementById("num1").value);
            const dos = parseInt(document.getElementById("num2").value);
            return uno + dos;
        }

        /**
         * Scope Var, const y let
         */


        let name = "tuto";
        var test = function () {
            let name = "Cony";
            console.log("name dentro del scope debiese ser Cony: " + name);
        }
        test();

        console.log("Fuera de scope name debiese ser tuto: " + name);


        /*
         * Arrays
         * 
         */

        const array = ["tut", "cony", "moto"];

        console.log("Primer elemento array (tut): " + array[0])


        console.log("Acceder a elemento por indice (moto): " + array[array.length - 1]);

        array.push("Tata");
        console.log("Agrege tata al ultimo en el arreglo: " + array);

        array.pop();
        console.log("Elimine el ultimo elemento del array: " + array);

        array.unshift("Primero");

        console.log("Agregar un elemento al prncipio:" + array);

        array.shift();

        console.log("elimine el primero: " + array);

        for (let index = 0; index < array.length; index++) {
            const element = array[index];
            console.log("Recorriendo Elemento: " + element + " - Indice: " + index);
        }


        const multidimencional = [
            [1, 2],
            [3, 4],
            [5, 6],
            [7, 8]
        ];
        console.log("Arreglo multidimencional : " + multidimencional);
        console.log("Arreglo multidimencional posicion 0 : " + multidimencional[0]);


        const concatenoArray = [].concat(...multidimencional);

        console.log("Arreglo multidimencional concatenado [].concat(multidimencional): " + concatenoArray);
        console.log("Arreglo multidimencional concatenado posicion 0: " + concatenoArray[0]);


        /*
         * Objetos
         * 
         * 
         */

        //number


        MasgrandeNum = Number.MAX_VALUE;
        MaspequeNum = Number.MIN_VALUE;
        infinitoNum = Number.POSITIVE_INFINITY;
        notInfinitoNum = Number.NEGATIVE_INFINITY;
        noesNum = Number.NaN;

        console.log("infinitoNUm Number.Positive_infinity :  " + infinitoNum + " - max_value: " + MasgrandeNum);


        //date



        var fecha = moment().format("DD/MM/YYYY");
        console.log("Fecha: " + fecha);